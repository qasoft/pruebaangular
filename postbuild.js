const fs = require('fs-extra');

const project = process.argv[2];
const destinationPath = `projects/host/src/assets/${project}`;

fs.moveSync(`dist/${project}/main.js`, `${destinationPath}/main.js`, {overwrite: true});
fs.moveSync(`dist/${project}/styles.css`, `${destinationPath}/styles.css`, {overwrite: true});