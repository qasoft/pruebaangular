import { Component, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'runt-component',
    template: '<div #contenedor></div>'
})
export class RuntComponent implements OnInit {
    constructor(private element: ElementRef, private router: Router, private route: ActivatedRoute){
        
    }

    ngOnInit(): void {
        this.route.data.subscribe(componente => {
            let element = document.createElement(componente.tag);
            Object.keys(componente.atributos || {})
                .forEach(key => element.setAttribute(key, componente.atributos[key]))


            this.element.nativeElement.appendChild(element);

            if (!customElements.get(componente.tag)){
                this.element.nativeElement.appendChild(script(componente.url));
            }

            if (componente.styles) {
                this.element.nativeElement.appendChild(styles(componente.styles));
            }
        });
    }
}

function script(url: string) {
    let node = document.createElement('script');
    node.src = url;
    node.type = 'text/javascript';
    node.async = true;

    return node;
}

function styles(url: string) {
    let node = document.createElement('link');
    node.href = url;
    node.rel = 'stylesheet';

    return node;
}