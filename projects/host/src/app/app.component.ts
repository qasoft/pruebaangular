import { EventoHost } from './eventos/eventos.model';
import { Subscription } from 'rxjs';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Id } from 'runt/bus-eventos';

@Component({
  selector: 'runt-root',
  template: `
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <span class="navbar-brand" href="#">Runt HostApp</span>
      <ul class="nav navbar-nav" routerLinkActive="active">
        <li class="nav-item"><a class="nav-link" routerLink="uno">Componente Uno</a></li>
        <li class="nav-item"><a class="nav-link" routerLink="dos">Componente Dos</a></li>
      </ul>
    </nav>
    <runt-tres></runt-tres>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent implements OnInit, OnDestroy {
  persona: any = {};
  subcripcionUno: Subscription;
  subcripcionDos: Subscription;

  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.subcripcionUno = window.Bus.subscribir(Id.of('EventoUno'), (e: any) => {
      this.persona = { ...this.persona, ...e.nombres }
      alert(`El Componente Uno dice: Nombres ${e.nombres.primerNombre}, ${e.nombres.segundoNombre}`);
      this.onGuardar();
    });

    this.subcripcionDos = window.Bus.subscribir(Id.of('EventoDos'), (e: any) => {
      this.persona = { ...this.persona, ...e.apellidos }
      alert(`El Componente Dos dice: Apellidos ${e.apellidos.primerApellido}, ${e.apellidos.segundoApellido}`);
      this.onGuardar();
    });
  }

  ngOnDestroy(): void {
    this.subcripcionUno?.unsubscribe();
    this.subcripcionDos?.unsubscribe();
  }

  onGuardar(): void {
    if (this.persona.primerNombre && this.persona.primerApellido) {
      window.Bus.publicar(EventoHost.with(this.persona));
    }
  }
}
