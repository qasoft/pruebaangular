import { Evento, Id } from 'runt/bus-eventos';

export class EventoHost<T> implements Evento {
  private static ID = Id.of('EventoHost');

  constructor(readonly contenido: T) { }

  get Id(): Id {
    return EventoHost.ID;
  }

  static with<T>(contenido: T): EventoHost<T> {
    return new EventoHost<T>(contenido);
  }
}