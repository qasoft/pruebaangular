import { RuntComponent } from './runt.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'uno', component: RuntComponent, data: { tag: 'runt-uno', atributos: {'nombre-usuario': 'RUNT'}, url: 'assets/uno/main.js', styles: 'assets/uno/styles.css'} },
  { path: 'dos', component: RuntComponent, data: { tag: 'runt-dos', url: 'assets/dos/main.js', styles: 'assets/dos/styles.css' } }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
