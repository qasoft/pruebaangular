import { EventoUno } from './app.component.model';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'runt-root',
  templateUrl: 'app.component.html',
  styles: []
})
export class AppComponent implements OnInit {
  @Input() nombreUsuario = 'uno';
  readonly formulario: FormGroup = new FormGroup({
    primerNombre: new FormControl(null, [Validators.required]),
    segundoNombre: new FormControl(null, [Validators.required])
  });

  ngOnInit(): void {
    
  }

  onGuardar(): void {
    window.Bus.publicar(EventoUno.with(this.formulario.value));
  }
}

