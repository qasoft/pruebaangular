import { Evento, Id } from 'runt/bus-eventos';

export interface NombresDTO {
    primerNombre: string;
    segundoNombre: string;
}

export class EventoUno implements Evento {
    private static ID: Id = Id.of('EventoUno');

    private constructor(readonly nombres: NombresDTO) { }

    get Id(): Id {
        return EventoUno.ID;
    }

    static with(nombres: NombresDTO): any {
        return new EventoUno(nombres);
    }
}