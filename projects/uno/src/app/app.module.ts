import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { ElementZoneStrategyFactory } from 'elements-zone-strategy';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  bootstrap: environment.production ? [] : [AppComponent],
  providers: []
})
export class AppModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const strategyFactory = new ElementZoneStrategyFactory(AppComponent, this.injector);
    const unoComponent = createCustomElement(AppComponent, {injector: this.injector, strategyFactory});
    customElements.define('runt-uno', unoComponent);
  }
 }
