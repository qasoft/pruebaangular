import { EventoDos } from './app.component.model';
import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'runt-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  @Input() nombreUsuario = 'uno';
  readonly formulario: FormGroup = new FormGroup({
    primerApellido: new FormControl(null, [Validators.required]),
    segundoApellido: new FormControl(null, [Validators.required])
  });

  ngOnInit(): void {
    
  }

  onGuardar(): void {
    window.Bus.publicar(EventoDos.with(this.formulario.value));
  }
}
