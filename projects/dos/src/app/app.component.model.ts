import { Evento, Id } from 'runt/bus-eventos';

export interface ApellidosDTO {
    primerApellido: string;
    segundoApellido: string;
}

export class EventoDos implements Evento {
    private static ID: Id = Id.of('EventoDos');

    private constructor(readonly apellidos: ApellidosDTO) { }

    get Id(): Id {
        return EventoDos.ID;
    }

    static with(apellidos: ApellidosDTO): any {
        return new EventoDos(apellidos);
    }
}