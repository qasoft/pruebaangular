import { Evento, Id } from './bus-eventos.model';
import { Subject, Subscription } from 'rxjs';

const subscripciones: { [key: string]: Subject<any> } = {};
export class BusEventos {

    subscribir<T extends Evento>(id: Id, accion: (e: T) => void): Subscription {
        let subject: Subject<T> = subscripciones[id.toString()];
        if (!subject) {
            subject = subscripciones[id.toString()] = new Subject<T>();
        }

        return subject.subscribe((e: T) => accion(e))
    }

    publicar<T extends Evento>(evento: T): void {
        const subject: Subject<T> = subscripciones[evento.Id.toString()];
        subject?.next(evento);
    }
}

declare global {
    interface Window { Bus: BusEventos; }
}
