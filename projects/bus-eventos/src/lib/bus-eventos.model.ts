export interface Evento {
    Id: Id;
}

export class Id {
    private constructor (private id: string) {
    }

    static of(id: string): Id {
        if (!id) {
            throw new Error('No puede ser nulo o estar vacio.')
        }
        return new Id(id);
    }

    toString(): string {
        return this.id;
    }
}
