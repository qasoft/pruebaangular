const path = require('path');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports = {
    mode: "production",
    entry: path.resolve(__dirname, './src/public-api.ts'),
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: /node_modules/,
        },
      ],
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
      alias: {
        "runt/bus-eventos": path.resolve(__dirname, '../bus-eventos/src/public-api'),
      }
    },
    output: {
      filename: 'main.js',
      path: path.resolve(__dirname, '../../dist/tres'),
    },
  };