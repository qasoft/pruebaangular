import { Id } from 'runt/bus-eventos';

const template = `
    <div class="card" style="text-align: center;">
    <div class="card-body">
        <h1 class="card-title"></h1>
    </div>
    </div>
`;

export default class NativoComponent extends HTMLElement {
    contenedor: HTMLDivElement;
    subcripcionUno: any;
    subcripcionDos: any;
    persona: any;

    constructor() {
        super();

        const shadow = this.attachShadow({ mode: 'open' });
        this.contenedor = document.createElement('div');
        this.contenedor.setAttribute("id", "contenedor");
        this.contenedor.setAttribute("class", "content");

        shadow.appendChild(this.contenedor);
    }

    static get observedAttributes(): Array<string> {
        return ['mostrar'];
    }

    connectedCallback() {
        this.updateContent();
        if (!!window.Bus) {
            this.subcripcionUno = window.Bus.subscribir(Id.of('EventoHost'), (evento: any) => {
                console.log(evento);

                this.persona = evento.contenido;
                this.updateContent();
            });
        }
    }

    updateContent(): void {
        if (!!this.persona) {
            this.contenedor.innerHTML = template;

            const h5 = this.contenedor.querySelector('h1');
            h5.innerHTML = `${this.persona.primerNombre} ${this.persona.segundoNombre} ${this.persona.primerApellido} ${this.persona.segundoApellido}`;
        } else {
            this.contenedor.innerHTML = '';
        }
    }
}

window.customElements.define('runt-tres', NativoComponent);